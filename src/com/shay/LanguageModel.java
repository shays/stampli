package com.shay;

import java.util.Set;

public class LanguageModel {
    private boolean canBeFinal;
    private Set<Character> followers;


    public LanguageModel(boolean canBeFinal, Set<Character> followers) {
        this.canBeFinal = canBeFinal;
        this.followers = followers;
    }

    public boolean isCanBeFinal() {
        return canBeFinal;
    }

    public boolean canFollow(Character follower) {
        return followers.contains(follower);
    }

    public Set<Character> getFollowers() {
        return followers;
    }

}
