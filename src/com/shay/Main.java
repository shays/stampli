package com.shay;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        Language language = new Language();
        Validator validator = new Validator();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                printHelp();
                String s = br.readLine();
                switch(Integer.parseInt(s)) {
                    case 1:
                        System.out.println("Provide rule in the form of <letter> <followers> <isFinal>");
                        language.addUserRule(br.readLine());
                        break;
                    case 2:
                        language.printRules();
                        break;
                    case 3:
                        System.out.println("Please provide a word to validate: ");
                        boolean ret = validator.isValid(language, br.readLine());
                        System.out.println(ret ? "==Valid!==\n" : "==Not valid!==\n");
                        break;
                    case 4:
                        ut_generateData(language);
                        System.out.println("Rules created...\n");
                        break;
                    case 5:
                        System.out.println("Bye...");
                        System.exit(0);
                    default:
                        System.out.println("Unknown Option. Please try again.\n");
                }

            } catch (Exception e) {
                System.out.println("Invalid input. Please try again");
            }
        }
    }

    private static void printHelp() {
        System.out.println("\n1. Add rule\n2. Print Rules\n3. Test Word\n4. Create language\n5. Bye..\n");
    }

    private static void ut_generateData(Language language) {
        try {
            language.addUserRule("a a,b,d true");
            language.addUserRule("b a,f false");
            language.addUserRule("c a true");
            language.addUserRule("k b,d,r false");
            language.addUserRule("g a,c,d true");

        }catch (Exception e) {
            System.out.println("Failed creating rules");
        }
    }
}
