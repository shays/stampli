package com.shay;

import java.util.*;

public class Language {
    // Assumption: if there is no rule for a letter, it is not allowed (whitelist)
    private Map<Character, LanguageModel> language = new HashMap<>();



    private LanguageModel getModel(Character letter) {
        return language.get(letter);
    }

    private void setRule(char letter, Set<Character> followers, boolean canBeFinal) {
        LanguageModel model = new LanguageModel(canBeFinal, followers);
        language.put(letter, model);
    }

    public void addUserRule(String ruleInput) throws Exception {
        String[] items = ruleInput.split(" ");

        Character letter = items[0].charAt(0);
        String[] followersInput = items[1].split(",");
        Set<Character> followers = new HashSet<>();

        for (String follower : followersInput) {
            followers.add(follower.charAt(0));
        }

        boolean canBeFinal = items[2].equalsIgnoreCase("true") ? true : false;
        setRule(letter,followers, canBeFinal);
    }

    public boolean canBeFinal(Character letter) {
        LanguageModel model =  getModel(letter);
        if (model == null) {
            return false;
        }
        return model.isCanBeFinal();
    }

    public boolean canBeFollower(Character letter, Character follower) {
        LanguageModel model =  getModel(letter);
        if (model == null) {
            return true;
        }

        return model.canFollow(follower);
    }

    public void printRules() {
        List<Character> letters = new ArrayList<>(language.keySet());
        Collections.sort(letters);

        String align = "| %-6s | %-56s | %-12s |%n";
        System.out.format("+--------+----------------------------------------------------------+--------------+%n");
        System.out.format("| Letter | Followers                                                | Can be Final |%n");
        System.out.format("+--------+----------------------------------------------------------+--------------+%n");

        for (Character letter : letters) {
            LanguageModel model = getModel(letter);
            System.out.format(align, letter, model.getFollowers(), model.isCanBeFinal() ? " True" : "False");
        }
    }
}
