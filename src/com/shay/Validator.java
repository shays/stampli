package com.shay;

public class Validator {

    public boolean isValid(Language language, String word) {
        Character lastLetter = word.charAt(word.length() - 1);
        if (!language.canBeFinal(lastLetter)) {
            return false;
        }

        for (int i = 0; i < word.length() - 1; i++) {
            Character letter = word.charAt(i);
            for (int j = i + 1; j < word.length(); j ++) {
                Character follower = word.charAt(j);
                if (!language.canBeFollower(letter, follower)) {
                    return false;
                }
            }
        }

        return true;
    }
}
